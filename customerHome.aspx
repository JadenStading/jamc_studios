﻿<%@ Page Language="C#" Inherits="jamc_studios.customerHome"%>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
    
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="faq.aspx">FAQ</a>
        </div>
        <div runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </div>
      </div>
    </div>
    
    <div class="body">
      <h1>User Home</h1>
      Name: <asp:Label runat="server" name="firstName" id="firstName"/>
      <asp:Label runat="server" name="lastName" id="lastName"/><br>
      email: <asp:Label runat="server" name="email" id="email"/><br>
      
      Home Address: <br>
      <asp:Label type="text" runat="server" name="home1" id="home1"/><br>
      <asp:Label type="text" runat="server" name="home2" id="home2"/><br>
      <asp:Label type="text" runat="server" name="homeState" id="homeState" size="3"/>
      <asp:Label type="text" runat="server" name="homeZip" id="homeZip" size="8"/><br><br>

      Billing Address: <br>
      <asp:Label type="text" runat="server" name="bill1" id="bill1"/><br>
      <asp:Label type="text" runat="server" name="bill2" id="bill2"/><br>
      <asp:Label type="text" runat="server" name="billState" id="billState" size="3"/>
      <asp:Label type="text" runat="server" name="billZip" id="billZip" size="8"/><br><br>

      <asp:Label type="checkbox" runat="server" name="tripLoggingOption" id="tripLoggingOption"/><br><br>

      <a href="profile.aspx">Edit Profile</a>

      <form runat="server">
        <p><asp:Button id="logoutButton" runat="server" text="Logout" OnClick="executeLogout"/>
      </form>

    </div>
    
  </div>
</html>
