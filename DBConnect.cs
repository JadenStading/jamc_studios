using System;
using System.IO;
using System.Data;
using MySql.Data.MySqlClient;

namespace jamc_studios
{
    class DBConnect
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;
        public bool connStatus = false;
        private bool hasBeenDisposedOf = false;

        //Constructor
        public DBConnect()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "localhost";
            database = "jamc";
            uid = "root";
            password = "jadenalexmosecraig";
            string connectionString;
            connectionString = $"SERVER={server};" +
                               $"DATABASE={database};" +
                               $"UID={uid};" +
                               $"PASSWORD={password};" +
                                "SslMode=none";

            connection = new MySqlConnection(connectionString);


            try
            {
                connection.Open();
                connStatus = true;
                //return true;
            }
            catch (MySqlException ex)
            {
                connStatus = false;
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        Console.WriteLine("Cannot connect to database.");
                        //MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Console.WriteLine("Invalid username/password");
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                //return false;
            }
        }

        ~DBConnect()
        {
            if(!hasBeenDisposedOf)
                CloseConnection();
        }

        public void Dispose()
        {
            CloseConnection();
            hasBeenDisposedOf = true;
        }

        //open connection to database
        private void OpenConnection()
        {

        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                //MessageBox.Show(ex.Message);
                return false;
            }
        }

        //Insert statement
        public bool Insert(string table, string values)
        {
            string query = $"INSERT INTO {table} VALUES ({values});";
            Console.WriteLine(query);
                      //open connection
            try
            {
                if (connStatus)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    //Execute command
                    cmd.ExecuteNonQuery();

                    //close connection
                    //this.
                    return true;
                }
                
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.ToString());
                throw ex;
            }
            return true;
        }

        public MySqlDataReader Pull(string table, string col, string condition, string condition2 = " ", string condition3 = " ", string special = " ")
        {
            MySqlDataReader reader;

            string query = $"Select * From {table} Where {col} = '{condition}' {condition2} {condition3} {special}";
            Console.WriteLine(query);

            if (connStatus)//this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                reader = cmd.ExecuteReader();


                //close connection
                //this.CloseConnection();
                return reader;
            }
            else
                return null;
        }

        public string PullCount(string selection, string table)
        {
            string query = $"Select {selection} From {table} ;";
            Console.WriteLine(query);

            if (connStatus)//this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                
                using (MySqlCommand cmd = new MySqlCommand(query, connection))
                {
                    Int32 tempInt32 = Convert.ToInt32(cmd.ExecuteScalar());
                    return tempInt32.ToString();
                }
                
            }
            return "";
        }

        public MySqlDataReader PullAll(string table)
        {
            MySqlDataReader reader;

            string query = $"Select * From {table} ;";
            Console.WriteLine(query);

            if (connStatus)//this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                reader = cmd.ExecuteReader();


                //close connection
                //this.CloseConnection();
                return reader;
            }
            else
                return null;
        }

        //Update statement
        public void Update(string table, string col, string updateValue ,string conditionCol, string conditionValue)
    {
        string query = $"UPDATE {table} SET {col} = '{updateValue}' WHERE {conditionCol} = '{conditionValue}';";
            Console.WriteLine(query);

        //Open connection
        if (connStatus)
        {
            //create mysql command
            MySqlCommand cmd = new MySqlCommand();
            //Assign the query using CommandText
            cmd.CommandText = query;
            //Assign the connection using Connection
            cmd.Connection = connection;

            //Execute query
            cmd.ExecuteNonQuery();
    }
}

        public void Delete(string table, string conditionCol, string conditionValue)
        {
            string query = $"Delete From {table} where '{conditionCol}'='{conditionValue}';";
            //Console.WriteLine(query);

            if (connStatus)
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = query;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();
            }
        }

        public void ClearTable(string table)
        {
            string query = $"Delete From {table};";
            //Console.WriteLine(query);

            if (connStatus)
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = query;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();
            }
        }

        public void ClearSpecificRows(string table, string rowCondition)
        {
            string query = $"Delete From {table} Where {rowCondition};";
            //Console.WriteLine(query);

            if (connStatus)
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = query;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();
            }
        }
    }//end class
}//end namespace