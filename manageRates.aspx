﻿<%@ Page Language="C#" Inherits="jamc_studios.manageRates" %>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
    
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="manager.aspx">Profile</a>
          |
          <a href="faq.aspx">FAQ</a>
          |
          <a href="manager.aspx">Controls</a>
        </div>
        <form runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </form>
      </div>
    </div>
  
    <div class="body">
      <h1>Manage Rates</h1>
      <form runat="server">
        Flat fee: $<asp:TextBox runat="server" name="flatFee" id="flatFee" size="8"/><br>
        Night rate: <asp:TextBox runat="server" name="nightRate" id="nightRate" size="8"/>%<br>
        Per Mile charge: $<asp:TextBox runat="server" name="perMile" id="perMile" size="8"/><br>
        Group Discount rate: <asp:TextBox runat="server" name="groupRate" id="groupRate" size="8"/>%<br>
        Group Size for Discout: <asp:TextBox runat="server" name="groupSize" id="groupSize" size="8"/>
        <br>
        <asp:Button id="submitButton" runat="server" Text="Save" OnClick="savePrices" />
        <asp:Button id="cancelButton" runat="server" Text="Cancel" OnClick="cancelPrices" />    
      </form>
    </div>
  
  </div>
</html>
