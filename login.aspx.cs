﻿using System;
using MySql.Data.MySqlClient;
using System.Web.UI;
namespace jamc_studios
{
    public partial class login : pageTemplate
    {
        public login()
        {
        }

        public void executeLogin(object sender, EventArgs args)
        {
           
            string createText = $"Hello and Welcome, {Request.Form["username"]} {Request.Form["password"]}!" + Environment.NewLine;
            string pass = "";
            DBConnect connection = new DBConnect();
            MySqlDataReader data = connection.Pull("Login", "username", Request.Form["username"]);

            
            if (Request.Form["password"].Length <= 0 || Request.Form["username"].Length <= 0)
            {
                //Error!
                string script = "***Missing Login Credentails!!!***";
                Response.Write("<script>alert('" + script + "')</script>");
            }
            else
            {
                if (connection.connStatus && data.Read() != false)
                {
                    pass = data.GetString("password");
                }

                if (pass != Request.Form["password"] || !connection.connStatus)
                {
                    //Error!
                    string script = "***Invalid Login!!!***";
                    Response.Write("<script>alert('" + script + "')</script>");
                }
                else
                {
                    //select UID approprate to user loging in
                    Response.Cookies["uid"].Value = $"{data.GetInt16("userId")}";
                    Response.Cookies["uid"].Expires = DateTime.Now.AddDays(1);

                    Response.Cookies["userType"].Value = $"{data.GetChar("userType")}";
                    Response.Cookies["userType"].Expires = DateTime.Now.AddDays(1);
                    Response.Redirect($"index.aspx", true); //sends user to home page, now logged in
                }

            }

            connection.Dispose();
            //Temporary code below:
            // System.IO.File.WriteAllText("OUTPUT.txt", createText);
        }
    }
}
