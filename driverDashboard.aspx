﻿<%@ Page Language="C#" Inherits="jamc_studios.driverDashboard"%>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>


  <div class="siteBackdrop">
    
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="driverProfile.aspx">Profile</a>
          |
          <a href="faq.aspx">FAQ</a>
        </div>
        <div runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </div>
      </div>
    </div>

    <div class="body">

        <form runat="server">
        <br>
      <h1>Driver Dashboard</h1>
        <select id="RouteSelection" name="RouteSelection">
          <option value="1">Route 1</option>
          <option value="2">Route 2</option>
          <option value="3">Route 3</option>
          <option value="4">Route 4</option>
          <option value="5">Route 5</option>
          <option value="6">Route 6</option>
        </select>
        <br>
       

        <p><asp:Button id="RefreshButton" runat="server" text="Select" OnClick="Refresh"/>
      <asp:Literal id="orderTable" runat="server" />
      
        <p><asp:Button id="logoutButton" runat="server" text="Logout" OnClick="executeLogout"/>
      </form>

    </div>

  </div>
</html>
