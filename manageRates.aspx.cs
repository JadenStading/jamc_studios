using System;
using MySql.Data.MySqlClient;

namespace jamc_studios
{
    public partial class manageRates : managerPage
    {

        protected System.Web.UI.WebControls.TextBox flatFee;
        protected System.Web.UI.WebControls.TextBox nightRate;
        protected System.Web.UI.WebControls.TextBox perMile;
        protected System.Web.UI.WebControls.TextBox groupRate;
        protected System.Web.UI.WebControls.TextBox groupSize;

        public manageRates()
        {
        }

        new protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            if (Request.Cookies["uid"] == null)
            {
                Response.Redirect("index.aspx", true);
                return;
            }

            DBConnect connection = new DBConnect();
            MySqlDataReader data = connection.Pull("Prices", "priceId", "1");

            if (!connection.connStatus || !data.Read())
            {
                Response.Write("<script>alert('***DB error***')</script>");
                Response.Redirect("index.aspx");
                connection.Dispose();
                return;
            }

            flatFee.Text = $"{data.GetDecimal("flatFee")}";
            nightRate.Text = (100 * (data.GetDecimal("nightRate") - 1)).ToString("G29");// value(1.25) - 1 = percent extra charged eg. 25% 
            perMile.Text = $"{data.GetDecimal("perMile")}";
            groupRate.Text = (100 * (1 - data.GetDecimal("groupRate"))).ToString("G29");// 1-percent = percent off eg 1 /.90 = 10% discount
            groupSize.Text = $"{data.GetInt32("groupSize")}";

            data.Close();

            connection.Dispose();
        }

        public void savePrices(object sender, EventArgs args)
        {
            string createText = $"Pricing Information submitted:" + Environment.NewLine +
                                  Request.Form["flatFee"] + Environment.NewLine +
                                  Request.Form["nightRate"] + Environment.NewLine +
                                  Request.Form["perMile"] + Environment.NewLine +
                                  Request.Form["groupRate"] + Environment.NewLine +
                                  Request.Form["groupSize"] + Environment.NewLine;

            DBConnect connection = new DBConnect();

            //error check rate input
            string temp = Request.Form["nightRate"];
            float nr = float.Parse(temp);
            if(nr >=1)
            {
                nr *= 0.01f;
            }
            nr = nr + 1.0f;

            temp = Request.Form["groupRate"];
            float gr = float.Parse(temp);
            if (gr >= 1)
            {
                gr *= 0.01f;
            }
            gr = 1.0f - gr;
            
            //update rows
            connection.Update("Prices", "flatFee", Request.Form["flatFee"], "priceId", "1");
            connection.Update("Prices", "nightRate", nr.ToString(), "priceId", "1");
            connection.Update("Prices", "perMile", Request.Form["perMile"], "priceId", "1");
            connection.Update("Prices", "groupRate", gr.ToString(), "priceId", "1");
            connection.Update("Prices", "groupSize", Request.Form["groupSize"], "priceId", "1");

            Console.WriteLine(createText);
            Response.Redirect("manager.aspx", true);

            connection.Dispose();
        }

        public void cancelPrices(object sender, EventArgs args)
        {
            Response.Redirect("manager.aspx", true);
        }
    }
}