using System;
using MySql.Data.MySqlClient;
using System.Web.UI;
using System.Text;

namespace jamc_studios
{
    public partial class driverDashboard : driverPage
    {
        public System.Web.UI.WebControls.Literal orderTable;
        int j = 1;
        public driverDashboard()
        {
        }

        new protected void Page_Load(Object sender, EventArgs e)
        {
            DBConnect connection = new DBConnect();
            base.Page_Load(sender, e);

            string systemTime = DateTime.Now.ToString("HHmmss");
            string systemDate = $"'{DateTime.Today.ToString("yyyy-MM-dd")}'";
            //construct special case
            //string special = ", Order by tripDate";
            //construct extra conditions
            string condition2 = " AND tripDate = ";
            condition2 += systemDate;
            string condition3  = "AND tripTime >= ";
            condition3 += systemTime;

            StringBuilder tableString = new StringBuilder();

            if (connection.connStatus)
            {
               
                // Select * From Orders Where routeNo = '1'  AND tripDate = 4/11/2018 AND tripTime >= 12;
                MySqlDataReader data = connection.Pull("Orders", "routeNo", (j-1).ToString());//, condition2);//, condition3, special); //for later: Request.Form("crap here***")
                tableString.AppendLine("<table>");
                tableString.AppendLine("<tr>");
                tableString.AppendLine($"<td>Date</td>");
                tableString.AppendLine($"<td>Time</td>");
                tableString.AppendLine($"<td>Route</td>");
                tableString.AppendLine($"<td>From</td>");
                tableString.AppendLine($"<td>To</td>");
                tableString.AppendLine("</tr>");

                int i = 0;
                while (data.Read())
                {
                    //get schedule columns
                    DateTime date = DateTime.Parse(data.GetString("tripDate"));
                    DateTime time = DateTime.Parse(data.GetString("tripTime"));
                    string route = data.GetString("routeNo");
                    string from = data.GetString("fromAddress");
                    string to = data.GetString("toAddress");

                    //put into 
                    if (i < 5)
                    {
                        tableString.AppendLine("<tr>");
                        tableString.AppendLine($"<td>{date.ToString("d")}</td>");
                        tableString.AppendLine($"<td>{time.ToString("h:mm tt")}</td>");
                        tableString.AppendLine($"<td>{route}</td>");
                        tableString.AppendLine($"<td>{from}</td>");
                        tableString.AppendLine($"<td>{to}</td>");
                        tableString.AppendLine("</tr>");

                        //list[i] = route + " " + date + " " + time + " " + from + " " + to;
                    }
                    else
                    {
                        break;
                    }

                    i++;
                }

                tableString.AppendLine("<table>");

                orderTable.Text = tableString.ToString();

                data.Close();
            }
            connection.Dispose();
        }

        public void Refresh(object sender, EventArgs e)
        {
            j = int.Parse(Request.Form["RouteSelection"]);
        
            Page_Load(sender,e);
        }
    }
}
