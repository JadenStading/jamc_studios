﻿using System;
using MySql.Data.MySqlClient;
using System.Web.UI;
using System.Text;

namespace jamc_studios
{
    public partial class driverPage : pageTemplate
    {
        public driverPage()
        {
        }

        new protected void Page_Load(object sender, EventArgs e)
        {
            DBConnect connection = new DBConnect();

            base.Page_Load(sender, e);

            if (Request.Cookies["uid"] == null || Request.Cookies["userType"].Value != "d")
            {
                Response.Redirect("index.aspx", true);
            }

            connection.Dispose();
        }
    }
}