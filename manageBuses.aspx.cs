﻿using System;
using MySql.Data.MySqlClient;
using System.Web.UI;
using System.Text;

namespace jamc_studios
{
    public partial class manageBuses : managerPage
    {


        public manageBuses()
        {
        }

        public System.Web.UI.WebControls.Literal driverTable;

        /*new protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            DBConnect connection = new DBConnect();


            MySqlDataReader data = connection.PullAll("DriverInfo");

            StringBuilder tableString = new StringBuilder();

            if (connection.connStatus)
            {
                tableString.AppendLine("<table>");
                tableString.AppendLine("<tr>");
                tableString.AppendLine($"<td>First Name</td>");
                tableString.AppendLine($"<td>Last Name</td>");
                tableString.AppendLine($"<td>UserID</td>");
                tableString.AppendLine($"<td>Phone Number</td>");
                tableString.AppendLine($"<td>Start Availability</td>");
                tableString.AppendLine($"<td>End Availability</td>");
                tableString.AppendLine("</tr>");

                while (data.Read())
                {
                    string firstName = data.GetString("firstName");
                    string lastName = data.GetString("lastName");
                    string userId = data.GetString("userId");
                    string phoneNo = data.GetString("PhoneNo");
                    string startAvail = data.GetString("availablityStart");
                    string endAvail = data.GetString("availablityEnd");


                    tableString.AppendLine("<tr>");
                    tableString.AppendLine($"<td>{firstName}</td>");
                    tableString.AppendLine($"<td>{lastName}</td>");
                    tableString.AppendLine($"<td>{userId}</td>");
                    tableString.AppendLine($"<td>{phoneNo}</td>");
                    tableString.AppendLine($"<td>{startAvail}</td>");
                    tableString.AppendLine($"<td>{endAvail}</td>");
                    tableString.AppendLine("</tr>");

                }

                tableString.AppendLine("<table>");

                driverTable.Text = tableString.ToString();

                data.Close();
            }
            else
            {
                string script = "***We apologize. Our server seems to be down.***";
                Response.Write("<script>alert('" + script + "')</script>");
            }
        }*/

        public void updateBus(object sender, EventArgs e) {
            DBConnect conection = new DBConnect();

            if (!conection.connStatus)
            {
                conection.Dispose();
                return; //show error?
            }

            conection.Update("Buses", "maintenance", ((Request.Form["underMaintainance"] == "on")?"1":"0"), "busNo", Request.Form["busSelection"]);
            conection.Dispose();
        }
    }
}