﻿using System;

namespace jamc_studios
{
    public partial class billing : pageTemplate
    {
        public billing()
        {
        }

        public void submitBillingInfo(object sender, EventArgs args)
        {
            string createText = $"Billing Information submitted:" + Environment.NewLine +
                                  Request.Form["address1"] + Environment.NewLine +
                                  Request.Form["address2"] + Environment.NewLine +
                                  Request.Form["zipCode"] + Environment.NewLine;
            //DBConnect connection = new DBConnect();
            //connection.Insert("users", $"'{Request.Form["username"]}', '{Request.Form["password"]}'");
            Console.WriteLine(createText);
            Response.Redirect("afterOrder.aspx", true);
        }

        public void cancelSubmission(object sender, EventArgs args)
        {
            string feedback = "Billing submission canceled";
            Console.WriteLine(feedback);
            Response.Redirect("index.aspx", true);
        }
    }
}