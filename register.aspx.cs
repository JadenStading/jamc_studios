﻿using System;
using MySql.Data.MySqlClient;

namespace jamc_studios
{
    public partial class register : pageTemplate
    {
        public register()
        {
        }

        public void registerUser(object sender, EventArgs args)
        {
            base.Page_Load(sender, args);

            string username = Request.Form["username"];
            string email = Request.Form["email"];
            string password = Request.Form["password1"];
            string confPass = Request.Form["password2"];
            bool logTrips = (Request.Form["tripLoggingOption"] == "on");  

            string createText = $"Registration Information submitted:" + Environment.NewLine +
                                  username + Environment.NewLine +
                                  email + Environment.NewLine +
                                  password + Environment.NewLine +
                                  confPass + Environment.NewLine +
                                  (logTrips ? "opt in" : "opt out") + Environment.NewLine;
            
            Console.WriteLine(createText);

            if (password != confPass)
            {
                string script = "***Passwords to not match!***";
                Response.Write("<script>alert('" + script + "')</script>");
                return;
            }
            else if (password.Length < 7)
            {
                string script = "***Password too short!***";
                Response.Write("<script>alert('" + script + "')</script>");
                return;
            }

            try
            {
                DBConnect connection = new DBConnect();
                connection.Insert("Login",
                                  $"0," + //userId
                                  $"'{username}'," + //username
                                  $"'{password}'," + //password
                                  $"'c'," + //usertype
                                  $"'{email}'"); //email
                
                MySqlDataReader data = connection.Pull("Login", "username", username);
                data.Read();
                int uid = data.GetInt16("userID");
                data.Close();

                connection.Insert("UserInfo",
                                  $"{uid}," + //uid
                                  $"'{username}'," + //firstName
                                  $"''," + //lastName
                                  $"0000000000000000," + //ccNum
                                  $"''," + //homeAddress1
                                  $"''," + //homeAddress2
                                  $"'SD'," + //homeState
                                  $"''," + //homeZip
                                  $"''," + //billAddress1
                                  $"''," + //billAddress2
                                  $"'SD'," + //billState
                                  $"''," + //billZip
                                  $"0," + //hanicap
                                  $"{(logTrips ? "1" : "0")}"); //tripLog
                //include handicap soon

                Response.Redirect($"login.aspx?uid={username}", true);
                connection.Dispose();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine($"Error code {ex.ErrorCode}");
            }
        }

    }
}