using System;
using MySql.Data.MySqlClient;
using System.Web.UI;

namespace jamc_studios
{
    public partial class driverProfile : driverPage
    {
        protected System.Web.UI.WebControls.TextBox name;
        protected System.Web.UI.WebControls.TextBox email;
        protected System.Web.UI.WebControls.TextBox phoneNo;
        protected System.Web.UI.WebControls.TextBox availStart;
        protected System.Web.UI.WebControls.TextBox availEnd;

        public driverProfile()
        {
        }

        new protected void Page_Load(object sender, EventArgs e)
        {
            DBConnect connection = new DBConnect();
            MySqlDataReader data = connection.Pull("DriverInfo", "userId", Request.Cookies["uid"].Value);

            if (connection.connStatus)
            {
                data.Read();
                string name = "";
                name += data.GetString("firstName");
                name += ' ';
                name += data.GetString("lastName");
                string phone = data.GetString("phoneNo");
                string availStart = data.GetString("availablityStart");
                string availEnd = data.GetString("availablityEnd");
                data.Close();

                data = connection.Pull("Login", "userId", Request.Cookies["uid"].Value);
                data.Read();
                string email = data.GetString("email");

                this.name.Text = name;
                this.email.Text = email;
                this.phoneNo.Text = phone;
                this.availStart.Text = availStart;
                this.availEnd.Text = availEnd;
            }

            connection.Dispose();
        }
        public void submitProfileInfo(object sender, EventArgs args)
        {
            string createText = $" Driver Profile Information submitted:" + Environment.NewLine +
                                  Request.Form["name"] + Environment.NewLine +
                                  Request.Form["email"] + Environment.NewLine +
                                  Request.Form["phoneNo"] + Environment.NewLine +
                                  Request.Form["availStart"] + Environment.NewLine +
                                  Request.Form["availEnd"] + Environment.NewLine;
            DBConnect connection = new DBConnect();
            connection.Update("DriverInfo", "firstName", Request.Form["name"], "userId", Request.Cookies["uid"].Value);
            connection.Update("Login", "email", Request.Form["email"], "userId", Request.Cookies["uid"].Value);
            connection.Update("DriverInfo", "phoneNo", Request.Form["phoneNo"], "userId", Request.Cookies["uid"].Value);
            connection.Update("DriverInfo", "availablityStart", Request.Form["availStart"], "userId", Request.Cookies["uid"].Value);
            connection.Update("DriverInfo", "availablityEnd", Request.Form["availEnd"], "userId", Request.Cookies["uid"].Value);

            Console.WriteLine(createText);
            Response.Redirect("driverDashboard.aspx", true);

            connection.Dispose();
        }

        public void cancelSubmission(object sender, EventArgs args)
        {
            string feedback = "Profile submission canceled";
            Console.WriteLine(feedback);
            Response.Redirect("index.aspx", true);
        }

    }
}
