﻿<%@ Page Language="C#" Inherits="jamc_studios.manageBuses"%>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="manager.aspx">Profile</a>
          |
          <a href="faq.aspx">FAQ</a>
          |
          <a href="manager.aspx">Controls</a>
        </div>
        <div runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </div>
      </div>
    </div>

    <div class="body">
      <h1>Manage Buses</h1>
      <br>
      <form runat="server">
        <select id="busSelection" name="busSelection">
          <option value="1">Bus 1</option>
          <option value="2">Bus 2</option>
          <option value="3">Bus 3</option>
          <option value="4">Bus 4</option>
          <option value="5">Bus 5</option>
          <option value="6">Bus 6</option>
        </select>
        <br>
        Check this box if the selected bus is currently under maintainance. <input type="checkbox" id="underMaintainance" name="underMaintainance"/>
        <br>
        <p><asp:Button id="updateBusButton" runat="server" text="Update" OnClick="updateBus"/>
      </form>
      <p><a href="manager.aspx">Back to Manager Dashboard</a>
    </div>
    
  </div>
</html>