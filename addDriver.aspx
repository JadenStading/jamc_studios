﻿<%@ Page Language="C#" Inherits="jamc_studios.addDriver" %>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="profile.aspx">Profile</a>
          |
          <a href="faq.aspx">FAQ</a>
          |
          <a href="manager.aspx">Controls</a>
        </div>
        <div class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </div>
      </div>
    </div>
    
    <div class="body">
      <h1>Add Driver</h1>
      <form runat="server">
        Username:<br/><input type="text" runat="server" name="username" id="username"/><br/>
        Email:<br/><input type="text" runat="server" name="email" id="email"/><br/>
        Password:<br/><input type="password" runat="server" name="password1" id="password1"/><br/>
        Confirm Passowrd:<br/><input type="password" runat="server" name="password2" id="password2"/><br/>
        Opt in to Trip Logging?<br/>Yes<input type="checkbox" runat="server" name="tripLoggingOption" id="tripLoggingOption"/><br/>
        <br>
        Name: <br><asp:TextBox runat="server" name="firstName" id="firstName"/>
        <asp:TextBox runat="server" name="lastName" id="lastName"/><br>
        Phone Number:<br>
        <asp:TextBox runat="server" name="phoneNo" id="phoneNo"/><br>
        
        <asp:Button id="submitButton" runat="server" Text="Submit" OnClick="registerDriver"/>
      </form>
    </div>
    
  </div>
</html>
