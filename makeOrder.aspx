﻿<%@ Page Language="C#" Inherits="jamc_studios.makeOrder" %>
<html>

  <head>
    <link rel="stylesheet" href="styles.css">
  </head>

  <script>
    var today = new Date();

  </script>


  <div class="siteBackdrop">

    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="faq.aspx">FAQ</a>
        </div>
        <div class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </div>
      </div>
    </div>






    <div class="body">
      <h1>Make Order</h1>
        <form runat="server">


        <script>
          today = new Date();
        </script>

        <!-- Find number of passengers store in 'numPassengers-->
        Number of passengers: <br><input type="number" name="numPassengers" id="numPassengers"><br><br />

        <!-- Date stored as yyyy-mm-dd-->
        Date of Order: <br><input type="date" name="date" id="date" min="today"><br><br />

        <!-- Get pickup time stored in pickupTime in hh:mm format-->
        Pickup Time: <br /><input type="time" name="pickupTime" id="pickupTime"><br /><br />

        <!-- Get start location -->
        Pickup Location(Address Number Street City, State): <br /><input type="text" name="pickupLoc" id="scriptBox"/><br /><br />


        <!-- Get end location -->
        Dropoff Location(Address Number Street City, State): <br /><input type="text" name="dropoffLoc" id="scriptBox" /><br /><br />

        <asp:Button id="submitButton" runat="server" Text="Place Order" OnClick="submitOrder"/>
        </form>

      <div id="googleMap" style="width:100%;height:400px;"></div>

      <script>
        function myMap() {
          var mapProp = {
            center: new google.maps.LatLng(44.31094, -96.79861),
            zoom: 14,
          };
          var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        }
      </script>

      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZoNcJkSDHX6dZAZRFPh4_2SKfTWgZ_O4&callback=myMap"></script>


    </div>

  </div>
    
</html>
