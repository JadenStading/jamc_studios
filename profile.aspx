﻿<%@ Page Language="C#" Inherits="jamc_studios.profile" %>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="profile.aspx">Profile</a>
          |
          <a href="faq.aspx">FAQ</a>
        </div>
        <div runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </div>
      </div>
    </div>
  
    <div class="body">
      <h1>User Profile</h1>
      <form runat="server">
        Name: <br><asp:TextBox runat="server" name="firstName" id="firstName"/>
        <asp:TextBox runat="server" name="lastName" id="lastName"/><br>
        email: <br><asp:TextBox runat="server" name="email" id="email"/><br>
        
        Home Address: <br>
        Line 1: <br><asp:TextBox type="text" runat="server" name="home1" id="home1"/><br>
        Line 2: <br><asp:TextBox type="text" runat="server" name="home2" id="home2"/><br>
        ZIP code: <asp:TextBox type="text" runat="server" name="homeZip" id="homeZip" size="8"/>
        State: <asp:TextBox type="text" runat="server" name="homeState" id="homeState" size="3"/><br>

        Billing Address: <br>
        Line 1: <br><asp:TextBox type="text" runat="server" name="bill1" id="bill1"/><br>
        Line 2: <br><asp:TextBox type="text" runat="server" name="bill2" id="bill2"/><br>
        ZIP code: <asp:TextBox type="text" runat="server" name="billZip" id="billZip" size="8"/>
        State: <asp:TextBox type="text" runat="server" name="billState" id="billState" size="3"/><br>
        
        Opt in to trip logging? <br>Yes<asp:CheckBox type="checkbox" runat="server" name="tripLoggingOption" id="tripLoggingOption"/>
        <br>
        <asp:Button id="button1" runat="server" Text="Submit" OnClick="submitProfileInfo" />
        <asp:Button id="cancelButton" runat="server" Text="Cancel" OnClick="cancelSubmission" />    
      </form>
    </div>

  </div>
</html>
