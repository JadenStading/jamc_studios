﻿<%@ Page Language="C#" Inherits="jamc_studios.manageDrivers"%>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="manager.aspx">Profile</a>
          |
          <a href="faq.aspx">FAQ</a>
          |
          <a href="manager.aspx">Controls</a>
        </div>
        <form runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </form>
      </div>
    </div>

    <div class="body">
      <h1>Manage Drivers</h1>
      <p><a href="addDriver.aspx">Add New Driver</a>
      <br>
      <br><asp:Literal runat="server" id="driverTable"/>
      <p><a href="manager.aspx">Back to Manager Dashboard</a>
    </div>
    
  </div>
</html>