﻿using System;
using MySql.Data.MySqlClient;
using System.IO;
using System.Net;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace jamc_studios
{
    public partial class makeOrder : pageTemplate
    {
        public makeOrder()
        {
            
        }

        public void submitOrder(object sender, EventArgs args)
        {
            //checks if the user is logged in
            if (Request.Cookies["uid"] == null || Request.Cookies["uid"].Value == "") {
                Response.Redirect("billing.aspx");
                return;
            }

            string url = $"https://maps.googleapis.com/maps/api/directions/json?origin= {Request.Form["pickupLoc"]} &destination= {Request.Form["dropoffLoc"]} &key=AIzaSyBZoNcJkSDHX6dZAZRFPh4_2SKfTWgZ_O4";

            WebRequest request = WebRequest.Create(url);

            WebResponse response = request.GetResponse();

            Stream data = response.GetResponseStream();

            StreamReader reader = new StreamReader(data);

            // json-formatted string from maps api
            string responseFromServer = reader.ReadToEnd();

            response.Close();

            dynamic result = JObject.Parse(responseFromServer);
            
            string status = result.status;

            // return if Google return is invalid
            if (status != "OK" || status == null)
            {
                string script = "***Route Error: Try again***";
                Response.Write("<script>alert('" + script + "')</script>");
                return;
            }


            if (Request.Form["pickupTime"] == "")
            {
                string script = "***Invalid Time Entered: Try again***";
                Response.Write("<script>alert('" + script + "')</script>");
                return;
            }


            if (Request.Form["date"] == "")
            {
                string script = "***Invalid Date Entered: Try again***";
                Response.Write("<script>alert('" + script + "')</script>");
                return;
            }


            string distance = result.routes[0].legs[0].distance.text;
            string timeEstimate = result.routes[0].legs[0].duration.text;
            string endLat = result.routes[0].legs[0].end_location.lat;
            string endLng = result.routes[0].legs[0].end_location.lng;
            string startLat = result.routes[0].legs[0].start_location.lat;
            string startLng = result.routes[0].legs[0].start_location.lng;
            

            Location startLocation;
            Location endLocation;


            startLocation.latitude = Convert.ToDouble(startLat);
            startLocation.longitude = Convert.ToDouble(startLng);

            endLocation.latitude = Convert.ToDouble(endLat);
            endLocation.longitude = Convert.ToDouble(endLng);

            // return if Location is out of bounds

            if((startLocation.latitude < 43.465266) || (startLocation.latitude > 44.485128) || (endLocation.latitude < 43.465266) || (endLocation.latitude > 44.485128) || (startLocation.longitude < -97.733104) || (startLocation.longitude > -96.524608) || (endLocation.longitude < -97.733104) || (endLocation.longitude > -96.524608)) {
                string script = "***Invalid Location Entered: Try again***";
                Response.Write("<script>alert('" + script + "')</script>");
                return;
            }

            
            //*
            Console.WriteLine(responseFromServer);
            Console.WriteLine(distance);
            Console.WriteLine(timeEstimate);
            Console.WriteLine(endLat);
            Console.WriteLine(endLng);
            Console.WriteLine(startLat);
            Console.WriteLine(startLng);
            Console.WriteLine(Request.Form["pickupTime"] + ' ' + Request.Form["date"]);
            //*/

			// Time format is hh:mm Date format is yyyy-mm-dd
            string pickupTD = Request.Form["date"] + ' ' + Request.Form["pickupTime"];

            
            OrderInformation orderInformation = new OrderInformation();
            orderInformation.userId = uint.Parse(Request.Cookies["uid"].Value);//one for now
            orderInformation.numberOfPassengers = ulong.Parse(Request.Form["numPassengers"]);
            orderInformation.dropOffPoint.latitude = startLocation.latitude;
            orderInformation.dropOffPoint.longitude = startLocation.longitude;
            orderInformation.pickUpPoint.latitude = endLocation.latitude;
            orderInformation.pickUpPoint.longitude = endLocation.longitude;
            orderInformation.startTime = NodaTime.LocalDateTime.FromDateTime(DateTime.Parse(pickupTD));//??? might be right?

            //validate billing info

            ShedAlgo.placeOrder(orderInformation);

            distance = distance.Substring(0, distance.Length - 3);
            //string isLong = "?long=" + ((int.Parse(distance) > 40) ? "1" : "0");

			Response.Redirect("afterOrder.aspx", true);
			
			
			
			
			
        }
    }
}