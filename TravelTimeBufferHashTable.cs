﻿using System;
using System.Collections.Generic;
using System.Text;
using NodaTime;
using alexander_evans_ADTs;

using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace jamc_studios
{
    public static class TravelTimeBufferHashTable
    {
        static HashTable<RoutesTimes> theTable = new HashTable<RoutesTimes>(1000);

        public static NodaTime.Duration getTravelTime(Location A, Location B)
        {
            RoutesTimes searchValue = new RoutesTimes();
            searchValue.start = A;
            searchValue.end = B;
            searchValue = theTable.Find(searchValue);
            if (searchValue != null)
                return searchValue.travelTime;
            else
            {
                NodaTime.Duration tempTime = getGoogleMapsTime(A, B);
                double tempDist = getGoogleMapsDistance(A, B);
                searchValue = new RoutesTimes();
                searchValue.start = A;
                searchValue.end = B;
                searchValue.travelTime = tempTime;
                searchValue.distance = tempDist;
                theTable.add(searchValue);
                return tempTime;
            }
        }

        public static double getTravelDistance(Location A, Location B)
        {
            RoutesTimes searchValue = new RoutesTimes();
            searchValue.start = A;
            searchValue.end = B;
            searchValue = theTable.Find(searchValue);
            if (searchValue != null)
                return searchValue.distance;
            else
            {
                NodaTime.Duration tempTime = getGoogleMapsTime(A, B);
                double tempDist = getGoogleMapsDistance(A, B);
                searchValue = new RoutesTimes();
                searchValue.start = A;
                searchValue.end = B;
                searchValue.travelTime = tempTime;
                searchValue.distance = tempDist;
                theTable.add(searchValue);
                return tempDist;
            }
        }

        static NodaTime.Duration getGoogleMapsTime(Location A, Location B)
        {
            string startLat = (A.latitude).ToString();
            string startLng = (A.longitude).ToString();
            string endLat = (B.latitude).ToString();
            string endLng = (B.longitude).ToString();

            string url = $"https://maps.googleapis.com/maps/api/directions/json?origin=  {startLat} , {startLng} &destination= {endLat}, {endLng}  &key=AIzaSyBZoNcJkSDHX6dZAZRFPh4_2SKfTWgZ_O4";

            WebRequest request = WebRequest.Create(url);

            WebResponse response = request.GetResponse();

            Stream data = response.GetResponseStream();

            StreamReader reader = new StreamReader(data);

            // json-formatted string from maps api
            string responseFromServer = reader.ReadToEnd();

            response.Close();

            dynamic result = JObject.Parse(responseFromServer);

            string timeEstimate = result.routes[0].legs[0].duration.text;

            string TimeConvert = null;

            foreach (char c in timeEstimate)
            {
                if (c != ' ')
                {
                    TimeConvert += c;
                }
                else
                    break;
            }

            // estimated time stored in minutes as a double
            double DTime = Convert.ToDouble(TimeConvert);

            return NodaTime.Period.FromMilliseconds((long)(DTime * 6000)).ToDuration();
            //the value replaces 60 (you can change the fromSesonds to fromminutes or hours,
            //or milliseconds, or whatever you need)
        }

        static double getGoogleMapsDistance(Location A, Location B)
        {
            string startLat = (A.latitude).ToString();
            string startLng = (A.longitude).ToString();
            string endLat = (B.latitude).ToString();
            string endLng = (B.longitude).ToString();

            string url = $"https://maps.googleapis.com/maps/api/directions/json?origin=  {startLat} , {startLng} &destination= {endLat}, {endLng}  &key=AIzaSyBZoNcJkSDHX6dZAZRFPh4_2SKfTWgZ_O4";

            WebRequest request = WebRequest.Create(url);

            WebResponse response = request.GetResponse();

            Stream data = response.GetResponseStream();

            StreamReader reader = new StreamReader(data);

            // json-formatted string from maps api
            string responseFromServer = reader.ReadToEnd();

            response.Close();

            dynamic result = JObject.Parse(responseFromServer);


            string distance = result.routes[0].legs[0].distance.text;

            string distanceConvert = null;

            foreach (char c in distance)
            {
                if (c != ' ')
                {
                    distanceConvert += c;
                }
                else
                    break;
            }

            double Ddistance = Convert.ToDouble(distanceConvert);

            return Ddistance;
        }

    }

}
