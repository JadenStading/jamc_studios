using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System;
using System.Text;
using NodaTime;
using alexander_evans_ADTs;

namespace jamc_studios
{
    public static class ShedAlgo
    {
        static bool inUse = false;
        enum ErrorCode { ALL_GOOD = 0, NO_ROUTE_FOUND };

        /// <summary>
        /// might not work due to const ADT
        /// </summary>
        static NodaTime.Duration MINSTRETCH = NodaTime.Period.FromMinutes(7).ToDuration();
        static int capacity = 0;

        static LinkedList<TimesStruct> timesLinkedList = new LinkedList<TimesStruct>();
        static LinkedList<RouteNode> routeLinkedList = new LinkedList<RouteNode>();

        static LinkedListNode<RouteNode> insertedPickUp;
        static LinkedListNode<RouteNode> insertedDropOff;
        static LinkedListNode<TimesStruct> insertedTimesStruct;

        /// <summary>
        /// Removes/Cancels upcoming orders with the orderID that is passed in.
        /// </summary>
        /// <param name="orderID"></param>
        public static void deleteUpcomingOrder(UInt32 orderID)
        {
            if (inUse)
                return;
            inUse = true;
            bool notFound = true;

            //searches all routes unless we found it already
            for (ulong i = 0; i < getNumberOfRoutes()  && notFound; i++)
            {
                //load the route into memory
                getDatabaseInputAndBuildTheLinkedLists(i);
                //set up a node to use to step through the buffer
                LinkedListNode<TimesStruct> currentTimesStruct = timesLinkedList.First;
                //check that the list is not empte
                if (currentTimesStruct != null)
                {
                    //go untill we hit the end of the list or find the node
                    while (currentTimesStruct.Next != null && currentTimesStruct.Value.orderId != orderID)
                    {
                        currentTimesStruct = currentTimesStruct.Next;//grab next node
                    }
                }

                if (currentTimesStruct.Value.orderId == orderID)//check if we found the nod or hit the end of list
                {
                    //remove the order from the buffer of upcoming order data and routes
                    routeLinkedList.Remove(currentTimesStruct.Value.pickUpPtr);
                    routeLinkedList.Remove(currentTimesStruct.Value.dropOffPtr);
                    timesLinkedList.Remove(currentTimesStruct);
                    //push to the database
                    setDatabaseOutputAndreleaseTheLinkedLists(i);
                    notFound = false;
                }
                else
                {
                    if(i==(getNumberOfRoutes()-1))
                    {
                        //optional not found error handline goes here
                    }
                }
            }
            inUse = false;
            return;
        }

        /// <summary>
        /// Creates an upcoming order based on the order information that gets passed into the system
        /// </summary>
        /// <param name="orderInformation"></param>
        /// <returns></returns>
        public static int placeOrder(OrderInformation orderInformation)
        {
            if (inUse)
                return 1;
            inUse = true;
            //start algorithm

            //convert input to RouteNode and TimeStruct format
            //RouteNode tempPickUp = new RouteNode();
            //RouteNode tempDropOff = new RouteNode();
            //TimesStruct tempTimesStruct = new TimesStruct();
            insertedPickUp = new LinkedListNode<RouteNode>(new RouteNode());
            insertedDropOff = new LinkedListNode<RouteNode>(new RouteNode());
            insertedTimesStruct = new LinkedListNode<TimesStruct>(new TimesStruct());

            //set up crossreferenceing & initial values:
            //set up th inserted pick up nodes inital values
            insertedPickUp.Value.location = orderInformation.pickUpPoint;
            insertedPickUp.Value.routeNodeType = RouteNodeType.PICKUP;
            insertedPickUp.Value.orderReference = insertedTimesStruct;
            insertedPickUp.Value.userId = orderInformation.userId;
            //insertedPickUp.Value.orderId = 1; //************************************************************
            //set up th inserted drop off nodes inital values
            insertedDropOff.Value.location = orderInformation.dropOffPoint;
            insertedDropOff.Value.routeNodeType = RouteNodeType.DROPOFF;
            insertedDropOff.Value.orderReference = insertedTimesStruct;
            insertedDropOff.Value.userId = orderInformation.userId;
            //insertedDropOff.Value.orderId = 1;//************************************************************
            //set up th inserted times struct nodes inital values
            insertedTimesStruct.Value.dropOffPtr = insertedDropOff;
            insertedTimesStruct.Value.startTime = orderInformation.startTime.InZoneStrictly(NodaTime.DateTimeZone.Utc).ToInstant();
            insertedTimesStruct.Value.endTime = insertedTimesStruct.Value.startTime + TravelTimeBufferHashTable.getTravelTime(insertedPickUp.Value.location, insertedDropOff.Value.location);
            insertedTimesStruct.Value.numberOfPassengers = orderInformation.numberOfPassengers;
            insertedTimesStruct.Value.pickUpPtr = insertedPickUp;
            insertedTimesStruct.Value.stretchTime = (TravelTimeBufferHashTable.getTravelTime(insertedPickUp.Value.location, insertedDropOff.Value.location) * 0.1);
            insertedTimesStruct.Value.userId = orderInformation.userId;
            //insertedTimesStruct.Value.orderId = 1;
            //End crossreferenceing & initial values:



            //get number of routes from DB
            ulong NUMBER_OF_ROUTES = getNumberOfRoutes();

            ErrorCode errorCode = ErrorCode.ALL_GOOD;
            bool insertPickupSuccessful = false;
            bool insertDropoffSuccessful = false;

            
            //start route loop (1-6)
            for (ulong currentRoute = 0; currentRoute < NUMBER_OF_ROUTES && errorCode == ErrorCode.ALL_GOOD && !insertDropoffSuccessful; currentRoute++)
            {

                //set/refresh the current route number
                insertedDropOff.Value.routeNo = (uint)currentRoute;
                insertedPickUp.Value.routeNo = (uint)currentRoute;
                insertedTimesStruct.Value.routeNo = (uint)currentRoute;

                //build linked lists from DB
                getDatabaseInputAndBuildTheLinkedLists(currentRoute);

                //insert Timestruct inplace
                LinkedListNode<TimesStruct> CurrentTimesStruct = timesLinkedList.First;
                if (!(CurrentTimesStruct == null))
                {
                    //step through to the correct start time insertion point
                    while (!(CurrentTimesStruct == null) && insertedTimesStruct.Value.startTime >= CurrentTimesStruct.Value.startTime)
                    {
                        CurrentTimesStruct = CurrentTimesStruct.Next;
                    }
                    if (CurrentTimesStruct != null) timesLinkedList.AddAfter(CurrentTimesStruct, insertedTimesStruct);
                    else timesLinkedList.AddLast(insertedTimesStruct);
                }
                else
                    timesLinkedList.AddFirst(insertedTimesStruct);//if the list is empty just do a dumb insertion



                bool exitPickupLoop = false;
                LinkedListNode<RouteNode> CurrentRouteNode = null;
                if (CurrentTimesStruct != null)
                    CurrentRouteNode = CurrentTimesStruct.Value.pickUpPtr;
                //start pickupnode loop
                do
                {
                    bool goodRoute = false;
                    bool tryAgain = true;
                    //insert pickup routenode relative to timestruct via reference

                    if (CurrentRouteNode != null)
                        routeLinkedList.AddAfter(CurrentRouteNode, insertedPickUp);
                    else
                        routeLinkedList.AddLast(insertedPickUp);


                    //run check for valid route(if it is the first node skip this check)
                    goodRoute = validateRoute(ref tryAgain);

                    if (!goodRoute && tryAgain)
                    {
                        //remove pickup node
                        routeLinkedList.Remove(insertedPickUp);
                        if (CurrentRouteNode != null)
                            CurrentRouteNode = CurrentRouteNode.Next;
                    }
                    else if (!goodRoute && !tryAgain)
                    {
                        //remove pickup node
                        routeLinkedList.Remove(insertedPickUp);
                        //remove order(timeStruct)
                        timesLinkedList.Remove(insertedTimesStruct);
                        exitPickupLoop = true;
                    }
                    else if (goodRoute && tryAgain)
                    {
                        //throw error
                    }
                    else
                    {
                        //We did it!  Finish the pickup node and get ready to check the drop off! :)
                        insertPickupSuccessful = true;
                        exitPickupLoop = true;
                    }

                //if check is successfull exit loop by setting insertSuccessful = true, 
                //else delete the pickup node and move the pointer one space later insert one space later Unless 
                } while (!exitPickupLoop);


                bool exitDropoffLoop = false;
                if (insertPickupSuccessful)
                {
                    CurrentRouteNode = insertedPickUp;
                    //dropoff node insert
                    do
                    {
                        bool goodRoute = false;
                        bool tryAgain = true;
                        //insert dropoff routenode relative to timestruct via reference
                        if (CurrentRouteNode != null)
                            routeLinkedList.AddAfter(CurrentRouteNode, insertedDropOff);
                        else
                            routeLinkedList.AddLast(insertedDropOff);


                        //run check for valid route(if it is the first node skip this check)
                        goodRoute = validateRoute(ref tryAgain);

                        if (!goodRoute && tryAgain)
                        {
                            //remove pickup node
                            routeLinkedList.Remove(insertedDropOff);
                            if (CurrentRouteNode != null)
                                CurrentRouteNode = CurrentRouteNode.Next;
                        }
                        else if (!goodRoute && !tryAgain)
                        {
                            //remove pickup & dropoff node
                            routeLinkedList.Remove(insertedPickUp);
                            routeLinkedList.Remove(insertedDropOff);
                            //remove order(timeStruct)
                            timesLinkedList.Remove(insertedTimesStruct);
                            exitPickupLoop = true;
                        }
                        else if (goodRoute && tryAgain)
                        {
                            //throw error
                        }
                        else
                        {
                            //dropoff was successful!  Yay!  
                            insertDropoffSuccessful = true;
                            exitPickupLoop = true;
                        }
                    } while (exitDropoffLoop);

                    if (insertDropoffSuccessful)
                    {
                        setDatabaseOutputAndreleaseTheLinkedLists(currentRoute);
                    }
                }
            }
            //if
            //call garbage collector?

            inUse = false;
            return 0;
        }//end place order 

        /// <summary>
        /// Checks if it is actually possible to drive this route in time
        /// </summary>
        /// <param name="tryAgain"></param>
        /// <returns></returns>
        private static bool validateRoute(ref bool tryAgain)
        {
            tryAgain = false;
            ulong currentPassengerCount = 0;

            LinkedList<RouteBundle> bufferList = new LinkedList<RouteBundle>();
            LinkedList<RouteBundleIndex> bufferListPairIndex = new LinkedList<RouteBundleIndex>();

            LinkedListNode<RouteNode> currentNode = routeLinkedList.First;
            while (currentNode != null)
            {
                LinkedListNode<RouteBundleIndex> insertedRouteBundleIndex = new LinkedListNode<RouteBundleIndex>(new RouteBundleIndex());
                bufferListPairIndex.AddLast(insertedRouteBundleIndex);
                if (currentNode.Value.routeNodeType == RouteNodeType.PICKUP)
                {
                    currentPassengerCount += currentNode.Value.orderReference.Value.numberOfPassengers;
                    LinkedListNode<RouteBundle> tempRouteBundle = insertedRouteBundleIndex.Value.pickUp = new LinkedListNode<RouteBundle>(new RouteBundle());
                    tempRouteBundle.Value.routeNode = currentNode;
                    NodaTime.Duration legTime = tempRouteBundle.Value.routeNode.Value.timeTraveledSoFar = NodaTime.Duration.FromSeconds(0) + NodaTime.Duration.FromMinutes(3);
                    tempRouteBundle.Value.routeNode.Value.orderReference.Value.routeBundleIndex = insertedRouteBundleIndex;
                    tempRouteBundle.Value.routeNode.Value.orderReference.Value.adjustedEndTime = tempRouteBundle.Value.routeNode.Value.orderReference.Value.startTime;

                    if (currentNode.Previous != null)
                    {
                        legTime = TravelTimeBufferHashTable.getTravelTime(currentNode.Previous.Value.location, currentNode.Value.location);
                    }
                    bufferList.AddLast(tempRouteBundle);
                    foreach (RouteBundle routeBundle in bufferList)
                    {
                        routeBundle.routeNode.Value.timeTraveledSoFar += legTime;
                    }
                    tempRouteBundle.Value.routeNode.Value.orderReference.Value.adjustedEndTime = getPreviousNodeTime(tempRouteBundle) + legTime;
                    if (tempRouteBundle.Value.routeNode.Value.orderReference.Value.adjustedEndTime > (tempRouteBundle.Value.routeNode.Value.orderReference.Value.startTime + MINSTRETCH) && tempRouteBundle.Value.routeNode.Value.orderReference.Value.adjustedEndTime > (tempRouteBundle.Value.routeNode.Value.orderReference.Value.startTime + tempRouteBundle.Value.routeNode.Value.orderReference.Value.stretchTime))
                    {
                        tryAgain = false;
                        return false;
                    }
                }
                else
                {
                    currentPassengerCount -= currentNode.Value.orderReference.Value.numberOfPassengers;

                    currentNode.Value.orderReference.Value.adjustedEndTime = currentNode.Value.orderReference.Value.startTime + (currentNode.Value.orderReference.Value.pickUpPtr.Value.timeTraveledSoFar);
                    if (currentNode.Value.orderReference.Value.adjustedEndTime > (currentNode.Value.orderReference.Value.endTime + currentNode.Value.orderReference.Value.stretchTime) && currentNode.Value.orderReference.Value.adjustedEndTime > (currentNode.Value.orderReference.Value.endTime + MINSTRETCH))
                    {
                        tryAgain = false;
                        return false;
                    }

                    NodaTime.Duration legTime = TravelTimeBufferHashTable.getTravelTime(currentNode.Previous.Value.location, currentNode.Value.location) + NodaTime.Duration.FromMinutes(3);
                    LinkedListNode<RouteNode> tempRouteNodePickupPair = currentNode.Value.orderReference.Value.pickUpPtr;
                    bufferList.Remove(tempRouteNodePickupPair.Value.orderReference.Value.routeBundleIndex.Value.pickUp);
                    foreach (RouteBundle routeBundle in bufferList)
                    {
                        routeBundle.routeNode.Value.timeTraveledSoFar += legTime;
                    }
                }

                currentNode = currentNode.Next;
            }//end route buffer build
            return true;
        }//end validator

        /// <summary>
        /// Gets the time of the last node pickup/dropoff
        /// </summary>
        /// <param name="tempRouteBundle"></param>
        /// <returns></returns>
        private static NodaTime.Instant getPreviousNodeTime(LinkedListNode<RouteBundle> tempRouteBundle)
        {
            if (tempRouteBundle.Value.routeNode.Value.routeNodeType == RouteNodeType.PICKUP)
            {
                return tempRouteBundle.Value.routeNode.Value.orderReference.Value.adjustedStartTime;
            }
            else
            {
                return tempRouteBundle.Value.routeNode.Value.orderReference.Value.adjustedEndTime;
            }
        }

        /// <summary>
        /// saves the entire route to the database
        /// </summary>
        /// <param name="routeToConfigure"></param>
        /// <returns></returns>
        private static int setDatabaseOutputAndreleaseTheLinkedLists(ulong routeToConfigure)
        {
            //cross-reference dereferencing algorithym
            dereferenceTheCrossReferencedLinkedLists();

            //database varibles
            DBConnect connection = new DBConnect();

            if (connection.connStatus)
            {
                connection.ClearSpecificRows("RouteNodeListing", $"routeNo={routeToConfigure}");

                connection.ClearSpecificRows("SchedulerQueue", $"routeNo={routeToConfigure}");
                

                LinkedListNode<TimesStruct> CurrentTimesStruct = timesLinkedList.First;
                while (CurrentTimesStruct != null)
                {
                    connection.Insert("SchedulerQueue",
                        CurrentTimesStruct.Value.orderId.ToString() + ',' +
                        CurrentTimesStruct.Value.userId.ToString() + ',' +
                        CurrentTimesStruct.Value.routeNo.ToString() + ',' +
                        CurrentTimesStruct.Value.orderNumber.ToString() + ',' +
                        CurrentTimesStruct.Value.startTime.ToUnixTimeTicks().ToString() + ',' +
                        CurrentTimesStruct.Value.adjustedStartTime.ToUnixTimeTicks().ToString() + ',' +
                        CurrentTimesStruct.Value.endTime.ToUnixTimeTicks().ToString() + ',' +
                        CurrentTimesStruct.Value.adjustedEndTime.ToUnixTimeTicks().ToString() + ',' +
                        CurrentTimesStruct.Value.stretchTime.TotalTicks.ToString()
                    );

                    CurrentTimesStruct = CurrentTimesStruct.Next;
                }

                LinkedListNode<RouteNode> CurrentRouteNode = routeLinkedList.First;
                while (CurrentRouteNode != null)
                {
                    string tempType;
                    if (CurrentRouteNode.Value.routeNodeType == RouteNodeType.DROPOFF)
                        tempType = "true";
                    else
                        tempType = "false";
                    
                    string tempStr = CurrentRouteNode.Value.orderId.ToString() + ',' +
                        CurrentRouteNode.Value.userId.ToString() + ',' +
                        CurrentRouteNode.Value.routeNo.ToString() + ',' +
                        CurrentRouteNode.Value.orderNumber.ToString() + ',' +
                        CurrentRouteNode.Value.nodeOrderNumber.ToString() + ',' +
                        CurrentRouteNode.Value.location.latitude.ToString() + ',' +
                        CurrentRouteNode.Value.location.longitude.ToString() + ',' +
                        CurrentRouteNode.Value.timeTraveledSoFar.TotalTicks.ToString() + ',' +
                        tempType;

                    connection.Insert("RouteNodeListing", tempStr);

                    CurrentRouteNode = CurrentRouteNode.Next;
                }                
            }
            else
            {
                connection.Dispose();
                return 1;
            }

            connection.Dispose();
            return 0;
        }

        /// <summary>
        /// loads the entire route from the database
        /// </summary>
        /// <param name="routeToConfigure"></param>
        /// <returns></returns>
        private static int getDatabaseInputAndBuildTheLinkedLists(ulong routeToConfigure)
        {
            routeLinkedList.Clear();
            timesLinkedList.Clear();

            //database varibles
            DBConnect connection = new DBConnect();
            MySqlDataReader data = connection.Pull("SchedulerQueue", "routeNo", routeToConfigure.ToString(), "ORDER BY orderNumber");

            //check connection status
            if (connection.connStatus)
            {
                //commence with queries
                while (data.Read())
                {
                    LinkedListNode<TimesStruct> tempTimesStruct = new LinkedListNode<TimesStruct>(new TimesStruct());
                    //get crap
                    tempTimesStruct.Value.orderId = data.GetUInt32("orderId");
                    tempTimesStruct.Value.userId = data.GetUInt32("userId");
                    tempTimesStruct.Value.routeNo = data.GetUInt32("routeNo");
                    tempTimesStruct.Value.orderNumber = data.GetUInt32("orderNumber");
                    tempTimesStruct.Value.startTime = NodaTime.Instant.FromUnixTimeTicks(data.GetInt64("startTime"));
                    tempTimesStruct.Value.adjustedStartTime = NodaTime.Instant.FromUnixTimeTicks(data.GetInt64("adjustedStartTime"));
                    tempTimesStruct.Value.adjustedEndTime = NodaTime.Instant.FromUnixTimeTicks(data.GetInt64("adjustedEndTime"));
                    tempTimesStruct.Value.endTime = NodaTime.Instant.FromUnixTimeTicks(data.GetInt64("endTime"));
                    tempTimesStruct.Value.adjustedEndTime = NodaTime.Instant.FromUnixTimeTicks(data.GetInt64("adjustedEndTime"));
                    tempTimesStruct.Value.stretchTime = NodaTime.Duration.FromTicks(data.GetDouble("stretchTime"));
                    timesLinkedList.AddLast(tempTimesStruct);
                }

                data.Close();
                data = connection.Pull("RouteNodeListing", "routeNo", routeToConfigure.ToString(), "ORDER BY nodeOrderNumber");

                while (data.Read())
                {
                    LinkedListNode<RouteNode> tempRouteNode = new LinkedListNode<RouteNode>(new RouteNode());
                    tempRouteNode.Value.orderId = data.GetUInt32("orderId");
                    tempRouteNode.Value.userId = data.GetUInt32("userId");
                    tempRouteNode.Value.orderNumber = data.GetUInt32("orderNumber");
                    tempRouteNode.Value.nodeOrderNumber = data.GetUInt32("nodeOrderNumber");
                    tempRouteNode.Value.routeNo = data.GetUInt32("routeNo");
                    tempRouteNode.Value.location.latitude = data.GetDouble("latitude");
                    tempRouteNode.Value.location.longitude = data.GetDouble("longitude");
                    tempRouteNode.Value.timeTraveledSoFar = NodaTime.Duration.FromTicks(data.GetDouble("timeTraveledSoFar"));
                    bool tempType = data.GetBoolean("routeNodeType");
                    if (tempType == true /*ROPOFF*/)
                        tempRouteNode.Value.routeNodeType = RouteNodeType.DROPOFF;
                    else
                        tempRouteNode.Value.routeNodeType = RouteNodeType.PICKUP;

                    routeLinkedList.AddLast(tempRouteNode);
                }
                ////these are the linked lists we need to build to from the database
                //routeLinkedList;
                //timesLinkedList;
                //this requires Jadens code interface knowlege


                //getroute capacity
                data.Close();
                data = connection.Pull("Buses", "busNo", routeToConfigure.ToString());
                if (data.Read())
                {
                    if (connection.connStatus)
                        capacity = int.Parse(data.GetString("capacity"));
                }
                else
                    capacity = 30;

            }
            else
            {
                connection.Dispose();
                return 1;//error condition
            }


            //cross refrencing algorithym goes here
            performCrossReferenceHookup();
            connection.Dispose();

            return 0;
        }

        /// <summary>
        /// returns th total number of routes/buses
        /// </summary>
        /// <returns></returns>
        private static ulong getNumberOfRoutes()
        {

            //database varibles

            DBConnect connection = new DBConnect();
            string data = connection.PullCount("COUNT(*)", "Buses");

            if (data != "")
            {
                connection.Dispose();
                return ulong.Parse(data);
            }
            else
            {
                connection.Dispose();
                return 6;
            }
        }

        /// <summary>
        /// establishes the references needed to do scheduleing not saved in the database
        /// </summary>
        private static void performCrossReferenceHookup()
        {
            //loop through all route nodes
            LinkedListNode<RouteNode> CurrentRouteNode = routeLinkedList.First;
            if (CurrentRouteNode != null)
                while (CurrentRouteNode != null)
                {
                    //loop through all orders/timestructs
                    LinkedListNode<TimesStruct> CurrentTimesStruct = timesLinkedList.First;
                    while (CurrentTimesStruct != null && CurrentRouteNode.Value.orderNumber != CurrentTimesStruct.Value.orderNumber)
                    {
                        CurrentTimesStruct = CurrentTimesStruct.Next;
                    }

                    //solve for the node type
                    if (CurrentRouteNode.Value.routeNodeType == RouteNodeType.PICKUP)
                    {
                        //set a reference to the Corect Route Node
                        CurrentTimesStruct.Value.pickUpPtr = CurrentRouteNode;
                    }
                    else
                    {
                        //set a reference to the Corect Route Node
                        CurrentTimesStruct.Value.dropOffPtr = CurrentRouteNode;
                    }


                    CurrentRouteNode.Value.orderReference = CurrentTimesStruct;
                    CurrentRouteNode = CurrentRouteNode.Next;
                }
            else
            {

            }
        }

        /// <summary>
        /// formats the scheduler data to be saved to the database
        /// </summary>
        private static void dereferenceTheCrossReferencedLinkedLists()
        {
            //set up times struct order numbers
            InitializeTimesLinkedListOrderNumbers();
            uint i = 0;
            //set temp timestruct order thing
            LinkedListNode<TimesStruct> CurrentTimesStruct = timesLinkedList.First;
            //loop through route nodes
            while (CurrentTimesStruct!=null)
            {
                //set order id based on times struct combined order value
                CurrentTimesStruct.Value.orderId = i;
                CurrentTimesStruct.Value.pickUpPtr.Value.orderId = i;
                CurrentTimesStruct.Value.dropOffPtr.Value.orderId = i;
                i++;
                CurrentTimesStruct = CurrentTimesStruct.Next;
            }

            i = 0;
            LinkedListNode<RouteNode> CurrentRouteNode = routeLinkedList.First;
            while (CurrentRouteNode != null)
            {
                CurrentTimesStruct = CurrentRouteNode.Value.orderReference;
                CurrentRouteNode.Value.nodeOrderNumber = i;
                CurrentRouteNode.Value.orderNumber = CurrentTimesStruct.Value.orderNumber;
                CurrentRouteNode = CurrentRouteNode.Next;
                i++;
            }
        }

        /// <summary>
        /// set up the order numbers
        /// </summary>
        private static void InitializeTimesLinkedListOrderNumbers()
        {
            uint i = 0;
            foreach (TimesStruct timesStruct in timesLinkedList)
            {
                timesStruct.orderNumber = i;
                i++;
            }
        }


    }//end shedAlgo Class

}//end namespace