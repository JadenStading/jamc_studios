﻿using System;
using MySql.Data.MySqlClient;

namespace jamc_studios
{
    public partial class managerPage : pageTemplate
    {
        public managerPage()
        {
        }

        new protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            if (Request.Cookies["uid"] == null || Request.Cookies["userType"].Value != "m")
            {
                Response.Redirect("index.aspx", true);
            }


            DBConnect connection = new DBConnect();
            MySqlDataReader data = connection.Pull("DriverInfo", "userId", Request.Cookies["uid"].Value);

            if (connection.connStatus)
            {

            }
            connection.Dispose();
        }
    }
}