﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using NodaTime;
using System;
using System.IO;

namespace jamc_studios
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            debuggerFunctionJAMC();

        }

        void debuggerFunctionJAMC()
        {
            //OrderInformation orderInformation = new OrderInformation();
            //orderInformation.userId = 1;
            //orderInformation.dropOffPoint.latitude = 100;
            //orderInformation.dropOffPoint.longitude = 100;
            //orderInformation.pickUpPoint.latitude = 100;
            //orderInformation.pickUpPoint.longitude = 100;
            //orderInformation.startTime = NodaTime.LocalDateTime.FromDateTime(DateTime.Parse("5/15/2018 14:00:00"));//??? might be right?
            //ShedAlgo.placeOrder(orderInformation);
        }
    }
}
