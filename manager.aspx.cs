using System;
using MySql.Data.MySqlClient;
using System.Web.UI;

namespace jamc_studios
{
    public partial class manager : managerPage
    {
        
        protected System.Web.UI.WebControls.Label flatFee;
        protected System.Web.UI.WebControls.Label nightRate;
        protected System.Web.UI.WebControls.Label perMile;
        protected System.Web.UI.WebControls.Label groupRate;
        protected System.Web.UI.WebControls.Label groupSize;

        public manager()
        {
        }

        new protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            DBConnect connection = new DBConnect();
            MySqlDataReader data;
            //data.Read();

            //string firstName = data.GetString("firstName");
            //string LastName = data.GetString("LastName");
            //string phoneNo = data.GetString("phoneNo");

            //data.Close();

            data = connection.Pull("Prices", "priceId", "0");

            data.Read();
            flatFee.Text = $"{data.GetDecimal("flatFee")}";
            nightRate.Text = (100 * (data.GetDecimal("nightRate") - 1)).ToString("G29");// value(1.25) - 1 = percent extra charged eg. 25% 
            perMile.Text = $"{data.GetDecimal("perMile")}";
            groupRate.Text = (100 * (1 - data.GetDecimal("groupRate"))).ToString("G29");// 1-percent = percent off eg 1 /.90 = 10% discount
            groupSize.Text = $"{data.GetInt32("groupSize")}";

            connection.Dispose();
        }


    }
}