﻿using System;
using System.Collections.Generic;
using System.Text;
using alexander_evans_ADTs;

namespace jamc_studios
{
    public class OrderInformation
    {
        public uint userId;
        public NodaTime.LocalDateTime startTime;          //start time passsed in
        public Location pickUpPoint;
        public Location dropOffPoint;
        public ulong numberOfPassengers;
    }

    class TimesStruct
    {
        public uint orderId;
        public uint userId;
        public uint routeNo;
        public uint orderNumber;       //Unique identifier
        public NodaTime.Instant startTime;          //start time passsed in
        public NodaTime.Instant adjustedStartTime;  //start time passsed in
        public NodaTime.Instant endTime;            //calculated from travel time directly from A->B via google API
        public NodaTime.Instant adjustedEndTime;    //calculated from time to travel rout segment
        public NodaTime.Duration stretchTime;        //calculated by checking whether (endTime-startTime)*0.1 > minStretch
        public ulong numberOfPassengers;
        public LinkedListNode<RouteNode> pickUpPtr;
        public LinkedListNode<RouteNode> dropOffPtr;
        public LinkedListNode<RouteBundleIndex> routeBundleIndex;
    }

    enum RouteNodeType { PICKUP, DROPOFF }

    class RouteNode
    {
        public uint orderId;
        public uint userId;
        public uint routeNo;
        public uint nodeOrderNumber;       //Unique identifier
        public uint orderNumber;
        public Location location;
        public RouteNodeType routeNodeType;
        public NodaTime.Duration timeTraveledSoFar;
        public LinkedListNode<TimesStruct> orderReference;
    }

    public class RoutesTimes : IHashable<RoutesTimes>, IValueFeildsEquatable<RoutesTimes>
    {
        public Location start;
        public Location end;
        public NodaTime.Duration travelTime;
        public double distance;

        public ulong GetHash(ulong key)
        {
            return start.GetHash(key);
        }

        public bool ElementEquals(RoutesTimes B)
        {
            return this.start.Equals(B.start) && this.end.Equals(B.end);
        }
    }

    class RouteBundleIndex
    {
        public LinkedListNode<RouteBundle> pickUp = null;
    }

    class RouteBundle
    {
        //public LinkedListNode<RouteBundleIndex> routeBundleIndex;
        public LinkedListNode<RouteNode> routeNode;
    }

    public struct Location : IValueFeildsEquatable<Location>, IHashable<Location>
    {
        public double latitude;
        public double longitude;

        public bool ElementEquals(Location B)
        {
            if (this.latitude == B.latitude && this.longitude == B.longitude)
            {
                return true;
            }
            else
                return false;
        }

        public ulong GetHash(ulong key)
        {
            ulong m1 = (ulong)(latitude * 1000);
            ulong m2 = (ulong)(longitude * 1000);

            return (m1 + m2) % key;
        }

    }

}