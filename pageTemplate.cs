﻿using System;
using MySql.Data.MySqlClient;
using System.Web.UI;
namespace jamc_studios
{
    public partial class pageTemplate : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label loginlabel;

        public pageTemplate()
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["uid"] != null && Request.Cookies["uid"].Value != "")
            {
                string uid = Request.Cookies["uid"].Value;
                //later, we will need to validate the uid and fetch the username
                DBConnect connection = new DBConnect();
                MySqlDataReader data = connection.Pull("Login", "userId", uid);
                data.Read();
                if (data.GetString("userType") == "m")
                    loginlabel.Text = $"<a href='manager.aspx'>{data.GetString("username")}</a>";
                else if (data.GetString("userType") == "d")
                    loginlabel.Text = $"<a href='driverDashboard.aspx'>{data.GetString("username")}</a>";
                else loginlabel.Text = $"<a href='customerHome.aspx'>{data.GetString("username")}</a>";

                connection.Dispose();
            }
            else loginlabel.Text = "<a href='login.aspx'>Sign In</a>";
        }

        public void executeLogout(object sender, EventArgs args)
        {
            Response.Cookies["uid"].Expires = DateTime.Now.AddDays(-1);
            Response.Redirect("index.aspx", true);
        }
    }
}