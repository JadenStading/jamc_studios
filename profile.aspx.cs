﻿using System;
using MySql.Data.MySqlClient;

namespace jamc_studios
{
    public partial class profile : pageTemplate
    {
        protected System.Web.UI.WebControls.TextBox firstName;
        protected System.Web.UI.WebControls.TextBox lastName;
        protected System.Web.UI.WebControls.TextBox email;
        protected System.Web.UI.WebControls.TextBox home1;
        protected System.Web.UI.WebControls.TextBox home2;
        protected System.Web.UI.WebControls.TextBox homeState;
        protected System.Web.UI.WebControls.TextBox homeZip;
        protected System.Web.UI.WebControls.TextBox bill1;
        protected System.Web.UI.WebControls.TextBox bill2;
        protected System.Web.UI.WebControls.TextBox billState;
        protected System.Web.UI.WebControls.TextBox billZip;
        protected System.Web.UI.WebControls.CheckBox tripLoggingOption;

        public profile()
        {
        }

        new protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);
            
            Console.WriteLine("entering Profile.aspx");

            if (Request.Cookies["uid"] == null)
            {
                Response.Redirect("index.aspx", true);
                return;
            }

            Console.WriteLine($"UID present: {Request.Cookies["uid"].Value}");

            DBConnect connection = new DBConnect();
            MySqlDataReader data = connection.Pull("UserInfo", "userId", Request.Cookies["uid"].Value);//Request.Cookies["uid"].Value);

            if (!connection.connStatus) {
                Response.Write("<script>alert('***DB error***')</script>");
                Response.Redirect("index.aspx");
                connection.Dispose();
                return;
            }

            if (data.Read()) // ready data
            {
                firstName.Text = data.GetString("firstName");
                lastName.Text = data.GetString("lastName");
                home1.Text = data.GetString("homeAddress1");
                home2.Text = data.GetString("homeAddress2");
                homeState.Text = data.GetString("homeState");
                homeZip.Text = data.GetString("homeZip");
                bill1.Text = data.GetString("billAddress1");
                bill2.Text = data.GetString("billAddress2");
                billState.Text = data.GetString("billState");
                billZip.Text = data.GetString("billZip");
                tripLoggingOption.Checked = (data.GetInt16("tripLog") != 0);
            }
            else
            {
                Console.WriteLine("The database lacks an entry for you");
            }

            data.Close();
            data = connection.Pull("Login", "userId", Request.Cookies["uid"].Value);
            data.Read();
            email.Text = data.GetString("email");

            connection.Dispose();
        }

        public void submitProfileInfo(object sender, EventArgs args)
        {
            string createText = $"Profile Information submitted:" + Environment.NewLine +
                                  Request.Form["billingName"] + Environment.NewLine +
                                  Request.Form["address1"] + Environment.NewLine +
                                  Request.Form["address2"] + Environment.NewLine +
                                  Request.Form["zipCode"] + Environment.NewLine +
                                  Request.Form["tripLoggingOption"] + Environment.NewLine;
            DBConnect connection = new DBConnect();
            connection.Update("UserInfo", "firstName", Request.Form["firstName"], "userId", Request.Cookies["uid"].Value);
            connection.Update("UserInfo", "lastName", Request.Form["lastName"], "userId", Request.Cookies["uid"].Value);
            //connection.Update("UserInfo", "ccNum", "", "userId", Request.Cookies["uid"].Value);
            connection.Update("UserInfo", "homeAddress1", Request.Form["home1"], "userId", Request.Cookies["uid"].Value);
            connection.Update("UserInfo", "homeAddress2", Request.Form["home2"], "userId", Request.Cookies["uid"].Value);
            connection.Update("UserInfo", "homeState", Request.Form["homeState"], "userId", Request.Cookies["uid"].Value);
            connection.Update("UserInfo", "homeZip", Request.Form["homeZip"], "userId", Request.Cookies["uid"].Value);
            connection.Update("UserInfo", "billAddress1", Request.Form["bill1"], "userId", Request.Cookies["uid"].Value);
            connection.Update("UserInfo", "billAddress2", Request.Form["bill2"], "userId", Request.Cookies["uid"].Value);
            connection.Update("UserInfo", "billState", Request.Form["billState"], "userId", Request.Cookies["uid"].Value);
            connection.Update("UserInfo", "billZip", Request.Form["billZip"], "userId", Request.Cookies["uid"].Value);
            //connection.Update("UserInfo", "handicap", "0", "userId", Request.Cookies["uid"].Value);
            connection.Update("UserInfo", "tripLog", (Request.Form["tripLoggingOption"] == "on") ? "1" : "0", "userId", Request.Cookies["uid"].Value);
            connection.Update("Login", "email", Request.Form["email"], "userId", Request.Cookies["uid"].Value);


            Console.WriteLine(createText);
            Response.Redirect("customerHome.aspx", true);

            connection.Dispose();
        }

        public void cancelSubmission(object sender, EventArgs args)
        {
            string feedback = "Profile submission canceled";
            Console.WriteLine(feedback);
            Response.Redirect("index.aspx", true);
        }
    }
}