﻿<%@ Page Language="C#" Inherits="jamc_studios.faq" %>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="faq.aspx">FAQ</a>
        </div>
        <form id="form1" runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </form>
      </div>
    </div>

    <div class="body">
      <p/>Here are some frequently asked questions and their answers:
        
      <p/><em>What areas do you serve?</em>
      <p/>Currently, we serve all areas within 2 miles of Brookings South Dakota and Sioux Fall South Dakota.
        
      <p/><em>When are your routes availible?</em>
      <p/>We serve bus routes 24/7 to all served areas, but additional fees will be incurred if the order calls for pickup between the hours of 9PM to 6AM Central Time.
        
      <p/><em>What weather conditions do your routes run in?</em>
      <p/>We operate fully in any weather conditions unless the State-provided road conditions are red. You can <a href="https://www.safetravelusa.com/sd/">visit their website</a> to check these road conditions.
    </div>

  </div>
</html>