﻿<%@ Page Language="C#" Inherits="jamc_studios.driverProfile"%>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="driverDashboard.aspx">Dashboard</a>
          |
          <a href="faq.aspx">FAQ</a>
        </div>
        <div runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </div>
      </div>
    </div>
    
    <div class="body">
      <h1>Driver Profile</h1>
      <form runat="server">
        Name: <br><asp:TextBox runat="server" name="name" id="name"/><br>
        Email: <br><asp:TextBox runat="server" name="email" id="email"/><br>
        Phone Number: <br><asp:TextBox type="text" runat="server" name="phoneNo" id="phoneNo"/><br>
        Availaility:
        <br>Start: <asp:TextBox type="text" runat="server" name="availStart" id="availStart"/><br>
        <br>End: <asp:TextBox type="text" runat="server" name="availEnd" id="availEnd"/><br>
    
        <asp:Button id="submitButton" runat="server" Text="Submit" OnClick="submitProfileInfo" />
        <asp:Button id="cancelButton" runat="server" Text="Cancel" OnClick="cancelSubmission" />    
      </form>
    </div>
  
  </div>
</html>