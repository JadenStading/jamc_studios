﻿using System;
using System.Collections.Generic;
using System.Text;
using NodaTime;

namespace alexander_evans_ADTs
{
    interface IHashable<T>
    {
        ulong GetHash(ulong key);
    }

    interface IValueFeildsEquatable<T>
    {
        bool ElementEquals(T B);
    }

    class HashTable<T> where T : class, IHashable<T>, IValueFeildsEquatable<T>
    {

        public enum ErrorCode { ALL_GOOD = 0, ERROR_HASHMOD_NEGATIVE };
        public ErrorCode errorCode = ErrorCode.ALL_GOOD;
        public ulong hashModulus { private set; get; }
        List<LinkedList<T>> hashTableIndex = new List<LinkedList<T>>();
        
        private HashTable()
        {

        }

        public HashTable(ulong hashModulusIn)
        {
            if (hashModulusIn >= 1)
            {
                hashModulus = hashModulusIn;
                for (ulong i = 0; i < hashModulus; i++)
                {
                    hashTableIndex.Add(new LinkedList<T>());
                }
            }
            else
            {
                errorCode = ErrorCode.ERROR_HASHMOD_NEGATIVE;
            }

        }

        public void add(T element)
        {
            int index = (int)element.GetHash(hashModulus);
            hashTableIndex[index].AddLast(element);
        }

        public T Find(T Element)
        {
            int index = (int)Element.GetHash(hashModulus);
            foreach (T currentElement in hashTableIndex[index])
            {
                if (currentElement.ElementEquals(Element))
                {
                    return currentElement;
                }
            }
            return null;

        }
    }
}
