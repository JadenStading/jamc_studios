﻿<%@ Page Language="C#" Inherits="jamc_studios.billing" %>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="faq.aspx">FAQ</a>
        </div>
        <form runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </form>
      </div>
    </div>
  
    <div class="body">
      <h1>Billing</h1>
      <form id="billingForm" runat="server">
        Address Line 1: <br><input type="text" runat="server" name="address1" id="address1"/><br>
        Address Line 2: <br><input type="text" runat="server" name="address2" id="address2"/><br>
        ZIP code: <br><input type="text" runat="server" name="zipCode" id="zipCode" /><br>
    
        <asp:Button id="button1" runat="server" Text="Submit" OnClick="submitBillingInfo" />
        <asp:Button id="cancelButton" runat="server" Text="Cancel" OnClick="cancelSubmission" />
      </form>
    </div>
  
  </div>
</html>