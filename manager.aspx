﻿<%@ Page Language="C#" Inherits="jamc_studios.manager"%>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="profile.aspx">Profile</a>
          |
          <a href="faq.aspx">FAQ</a>
          |
          <a href="manager.aspx">Controls</a>
        </div>
        <div runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </div>
      </div>
    </div>
  
    <div class="body">
      <h1>Manager Dashboard</h1>
      
      Flat fee: $<asp:Label runat="server" name="flatFee" id="flatFee" size="8"/><br>
      Night rate: <asp:Label runat="server" name="nightRate" id="nightRate" size="8"/>%<br>
      Per Mile charge: $<asp:Label runat="server" name="perMile" id="perMile" size="8"/><br>
      Group Discount rate: <asp:Label runat="server" name="groupRate" id="groupRate" size="8"/>%<br>
      Group Size for Discout: <asp:Label runat="server" name="groupSize" id="groupSize" size="8"/>
      <br>

      <p><a href="manageRates.aspx">Rates</a>
      <p><a href="manageDrivers.aspx">Drivers</a>
      <p><a href="manageOrders.aspx">Orders</a>
      <p><a href="manageBuses.aspx">Buses</a>

      <form runat="server">
        <p><asp:Button id="logoutButton" runat="server" text="Logout" OnClick="executeLogout"/>
      </form>

    </div>
    
  </div>
</html>