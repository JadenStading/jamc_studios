using System;
using MySql.Data.MySqlClient;
using System.Web.UI;
namespace jamc_studios
{
    public partial class customerHome : pageTemplate
    {
        protected System.Web.UI.WebControls.Label firstName;
        protected System.Web.UI.WebControls.Label lastName;
        protected System.Web.UI.WebControls.Label email;
        protected System.Web.UI.WebControls.Label home1;
        protected System.Web.UI.WebControls.Label home2;
        protected System.Web.UI.WebControls.Label homeState;
        protected System.Web.UI.WebControls.Label homeZip;
        protected System.Web.UI.WebControls.Label bill1;
        protected System.Web.UI.WebControls.Label bill2;
        protected System.Web.UI.WebControls.Label billState;
        protected System.Web.UI.WebControls.Label billZip;
        protected System.Web.UI.WebControls.Label tripLoggingOption;

        public customerHome()
        {
        }

        new protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            DBConnect connection = new DBConnect();

            if (connection.connStatus)
            {
                MySqlDataReader data = connection.Pull("UserInfo", "userId", Request.Cookies["uid"].Value);
                data.Read();
                firstName.Text = data.GetString("firstName");
                lastName.Text = data.GetString("lastName");
                home1.Text = data.GetString("homeAddress1");
                home2.Text = data.GetString("homeAddress2");
                homeZip.Text = data.GetString("homeZip");
                homeState.Text = data.GetString("homeState");
                bill1.Text = data.GetString("billAddress1");
                bill2.Text = data.GetString("billAddress2");
                billZip.Text = data.GetString("billZip");
                billState.Text = data.GetString("billState");
                tripLoggingOption.Text = $"You have opted {((data.GetInt16("tripLog") == 1) ? "in" : "out")} of trip logging.";
                data.Close();

                data = connection.Pull("Login", "userId", Request.Cookies["uid"].Value);
                data.Read();
                email.Text = data.GetString("email");
                data.Close();
            }
            else
            {
                string script = "***We apologize. Our server seems to be down.***";
                Response.Write("<script>alert('" + script + "')</script>");
            }

            connection.Dispose();
        }//end page load
    }//end page code
}//end class