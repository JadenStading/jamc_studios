﻿<%@ Page Language="C#" Inherits="jamc_studios.index" %>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
	        <a href="index.aspx">HOME</a>
	        |
	        <a href="makeOrder.aspx">Place Order</a>
	        |
	        <a href="faq.aspx">FAQ</a>
        </div>
        <form id="form1" runat="server" class="rightMenuItems">
          
	        <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </form>
      </div>
    </div>
    
    <div class="body">
      <h1>Ride R' Us</h1>
      <p>Thank you for visiting!
      <p>Looking for a reliable transportaion option between Brookings and Sioux Falls? Need a ride back home after droping off the lemon-car at the shop? Look no further than Ride R' Us! We have buses running 24/7 ready to service anyplace in the neighborhood. Just give us a call, and we guarantee that we'll be there: "From Point A, to Point B, and everywhere inbetween!"
      <br>
      <p><a href="register.aspx">Register now</a> to get notifications and speed up the order process!

    </div>
  
  </div>
</html>
