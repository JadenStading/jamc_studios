﻿<%@ Page Language="C#" Inherits="jamc_studios.afterOrder"%>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="faq.aspx">FAQ</a>
        </div>
        <form runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </form>
      </div>
    </div>
  
    <div class="body">
      <h1>Order Successful</h1>
      <p><a href="index.aspx">Go Home</a>
    </div>
    
  </div>
</html>