﻿<%@ Page Language="C#" Inherits="jamc_studios.login" %>
<html>
  <head>
    <link rel="stylesheet" href="styles.css">
  </head>
  
  <script src="loginScript.js"></script>
  
  <div class="siteBackdrop">
  
    <div class="menu">
      <div class="menuCenterpiece">
        <div class="leftMenuItems">
          <a href="index.aspx">HOME</a>
          |
          <a href="makeOrder.aspx">Place Order</a>
          |
          <a href="faq.aspx">FAQ</a>
        </div>
        <div runat="server" class="rightMenuItems">
          <asp:Label id="loginlabel" runat="server" text="<a href='login.aspx'>Sign In</a>"/>
        </div>
      </div>
    </div>
  
    <div class="body">
      <h1>Login</h1>
      <form id="form1" runat="server">
        Username: <br><input type="text" runat="server" name="username" id="username"/><br>
        Password: <br><input type="password" runat="server" name="password" id="password"/><br>
        <asp:Button id="button1" runat="server" Text="Login" OnClick="executeLogin" />
      </form>

      <br>Don't have an account? <a href="register.aspx">Register now!</a>
    </div>

  </div>
</html>
